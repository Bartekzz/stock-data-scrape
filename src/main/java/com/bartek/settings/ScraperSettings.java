package com.bartek.settings;

import com.bartek.util.*;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;

public class ScraperSettings {

    public static ScraperSettings instanceOfScraperSettings = null;
    private static List<String> whiteListUrls;
    private static BrowserMobProxyServer server;

    // static method to create instance of class ScraperSettings
    public static ScraperSettings getInstanceOfScraperSettings(List<String> whiteList) throws UnknownHostException
    {
        whiteListUrls = whiteList;
        if (instanceOfScraperSettings == null)
        {
            System.out.println("New instance of ScraperSettings!");
            instanceOfScraperSettings = new ScraperSettings();
            //startBrowserMobProxy();
        }

        System.out.println("Reusing instance of ScraperSettings.");
        return instanceOfScraperSettings;
    }


    public void startBrowserMobProxy() throws UnknownHostException
    {
        server = new BrowserMobProxyServer();
        server.setTrustAllServers(true);
        server.setChainedProxy(new InetSocketAddress(InetAddress.getByName("127.0.0.1"), 24001));
        server.start();
    }

    public void stopBrowserMobProxy() {
        server.stop();
    }

    public WebDriver init() throws Exception {

        //Set sleep
        int sleepMin = 1;
        int sleepMax = 3;

        // Set proxy
        boolean proxyIsEnabled = false;

        String proxyIp = "";
        String user = "";
        String password = "";
        String proxy = "";
        int port = 0;

        // Check ip via Aws
        boolean checkIp = false;

        // Local proxy settings
        boolean proxyLocal = false;

        if (proxyLocal) {
            user = "lum-customer-hl_ede7fb2d-zone-static-country-gb";
            password = "****";
            proxy = "zproxy.lum-superproxy.io";
            port = 22225;
        }

        // Proxy Manager
        boolean proxyManager = false;

        if (proxyManager) {
            proxy = "127.0.0.1";
            port = 24001;
        }

        //BroserMobProxy

        boolean browserMobProxy = true;

        // Set os
        final String os = "pc";


        if (os.equals("mac")) {
            System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
        }
        if (os.equals("ubuntu")) {
            System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
        }
        if (os.equals("pc")) {
            System.setProperty("webdriver.gecko.driver", "c:\\gecko\\geckodriver.exe");
        }

        FirefoxProfile profile = new FirefoxProfile();

        if (proxyIsEnabled) {
            //profile.setPreference("network.proxy.type", 1);
            //profile.setPreference("network.proxy.http", proxy);
            //profile.setPreference("network.proxy.http_port", port);
            //profile.setPreference("network.proxy.ssl", proxy);
            //profile.setPreference("network.proxy.ssl_port", port);
            //profile.setPreference("signon.autologin.proxy", true);
            profile.setPreference("devtools.jsonview.enabled", false);

            profile.setPreference("media.peerconnection.enabled", false);
            profile.setPreference("privacy.donottrackheader.enabled", true);
            profile.setPreference("plugin.state.flash", 0);

            profile.setPreference("app.update.auto", false);
            profile.setPreference("app.update.enabled", false);
            profile.setPreference("app.update.silent", false);
            profile.setPreference("app.update.staging.enabled", false);
            profile.setPreference("browser.search.update", false);
            profile.setPreference("app.update.service.enabled", false);
            profile.setPreference("app.update.staging", false);
            profile.setPreference("app.update.url", "");

            profile.setPreference("media.gmp-manager.cert.requireBuiltIn", false);
            profile.setPreference("media.gmp-manager.cert.checkAttributes", false);
            profile.setPreference("media.gmp-manager.updateEnabled", false);
            profile.setPreference("media.gmp-provider.enabled", false);
            profile.setPreference("media.gmp-widevinecdm.enabled", false);
            profile.setPreference("media.gmp-widevinecdm.visible", false);
            profile.setPreference("media.gmp.trial-create.enabled", false);
            profile.setPreference("media.gmp-manager.url", "");

            profile.setPreference("browser.download.lastDir", "downloadFile");
            profile.setPreference("browser.download.dir", "");
            profile.setPreference("browser.safebrowsing.downloads.remote.enabled", false);
            profile.setPreference("browser.safebrowsing.provider.mozilla.gethashURL", "");
            profile.setPreference("browser.safebrowsing.provider.mozilla.updateURL", "");
            profile.setPreference("browser.search.update", false);

            profile.setPreference("extensions.update.enabled", false);
            profile.setPreference("extensions.blocklist.enabled", false);
            profile.setPreference("extensions.systemAddon.update.enabled", false);
            profile.setPreference("extensions.update.autoUpdateDefault", false);
            profile.setPreference("extensions.systemAddon.update.url", "");

        }


        FirefoxOptions firefoxOptions = new FirefoxOptions();
        FirefoxBinary firefoxBinary = null;

        if (os.equals("mac")) {
            firefoxBinary = new FirefoxBinary(new File("/Applications/Firefox62.app/Contents/MacOS/firefox-bin"));
            firefoxOptions.setBinary("/Applications/Firefox62.app/Contents/MacOS/firefox-bin");
        }
        if (os.equals("ubuntu")) {
            firefoxBinary = new FirefoxBinary(new File("/usr/local/bin/firefox/firefox"));
            firefoxOptions.setBinary("/usr/local/bin/firefox/firefox");
        }
        if (os.equals("pc")) {
            firefoxBinary = new FirefoxBinary(new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe"));
            firefoxOptions.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
        }

        if(browserMobProxy)
        //if(browserMobProxy && (server != null || !server.isStarted()))
        {
            startBrowserMobProxy();
            System.out.println("Reusing BrowserMobProxy...");
            server.addRequestFilter((request, contents, messageInfo) -> {
                request.headers().add("Content-Type", "application/json");
                request.headers().add("EntitlementToken", "cecc4267a0194af89ca343805a3e57a");
                System.out.println(request.headers().entries().toString());
                return null;
            });

            server.whitelistRequests(whiteListUrls, 200);
            org.openqa.selenium.Proxy seleniumProxy = ClientUtil.createSeleniumProxy(server);
            seleniumProxy.setSslProxy("localhost:" + server.getPort());

            firefoxOptions.setProxy(seleniumProxy);
        }


        firefoxBinary.addCommandLineOptions("--headless");
        firefoxOptions.setCapability("marionette", true);
        //firefoxOptions.setLogLevel(FirefoxDriverLogLevel.TRACE);
        firefoxOptions.setBinary(firefoxBinary);
        firefoxOptions.setProfile(profile);

        // Get proxy ip
        WebDriver driver = new FirefoxDriver(firefoxOptions);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // Utils
        ScrapeUtilNew scrapeUtilNew = new ScrapeUtilNew();

        if (proxyLocal) {

            try {
                System.out.println("trying to wait");
                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                System.out.println("Handling alert");
                alert.sendKeys(user + Keys.TAB.toString() + password);
                alert.accept();
            }
            // Extend these exceptions to TriggerExcpetion
            catch (org.openqa.selenium.TimeoutException er) {
                er.printStackTrace();
                System.out.println("TimeoutException");
                driver.close();
                throw new Exception();
            } catch (NoAlertPresentException e) {
                e.printStackTrace();
                driver.close();
                System.out.println("NoAlertPresentException");
                throw new Exception();
            } catch (UnhandledAlertException ex) {
                ex.printStackTrace();
                driver.close();
                System.out.println("UnhandledAlertException");
                throw new Exception();
            }

        }

        if (proxyIsEnabled && checkIp) {
            driver.get("http://checkip.amazonaws.com/");
            // Get ip
            WebElement proxyIpElement = scrapeUtilNew.scrapeElementByTagName(driver, "pre");
            if (proxyIpElement != null) {
                proxyIp = proxyIpElement.getText();
                System.out.println("Proxy ip: " + proxyIp);
            }
        }

        return driver;

    }

    public static void main(String[] args) throws Exception {

        ScraperSettings scraperSettings = new ScraperSettings();
        scraperSettings.init();
    }
}