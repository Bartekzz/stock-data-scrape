package com.bartek.scrape.nasdaq;

import com.bartek.model.common.StockCompany;
import com.bartek.model.common.StockList;
import com.bartek.mongodb.Mongo;
import com.bartek.settings.ScraperSettings;
import com.bartek.util.ScrapeUtilNew;
import com.bartek.util.SerializationUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ScrapeStockCompaniesNasdaq
{

    private Mongo mongo;
    ScraperSettings scraperSettings;

    public ScrapeStockCompaniesNasdaq()
    {
        // Init DB
        mongo = Mongo.getMongoClientLocal();
        mongo.getMongoDatabase("stockdata");
    }

    public void getStockPrices() throws Exception
    {

        List<StockList> stockList = SerializationUtil.stockListFromJson("stocklists.json");

        ArrayList whiteList = new ArrayList();
        whiteList.add("http://www.nasdaqomxnordic.com/.*");
        for (StockList list : stockList)
        {
            System.out.println("Scraping list: " + list.getList());

            scraperSettings = new ScraperSettings().getInstanceOfScraperSettings(whiteList);
            WebDriver driver = scraperSettings.init();

            // Ohter settings (and exported to object)
            ScrapeUtilNew scrapeUtilNew = new ScrapeUtilNew();

            // Set site
            String site = list.getUrl();

            // Start scrape
            try
            {
                driver.get(site);

                List<WebElement> rows = scrapeUtilNew.scrapeIfElementsPresentByXpath(driver, 5,
                        "//*[@id='listedCompanies']/tbody/tr", true);

                List<StockCompany> stockCompanyList = new ArrayList<>();

                if (rows != null)
                {
                    for (WebElement row : rows)
                    {
                        StockCompany stockCompany = new StockCompany();

                        WebElement companyNameElement = row.findElement(By.xpath("./td/a"));
                        String companyName = companyNameElement.getText();
                        System.out.print(companyName + ", ");

                        WebElement companyCurrencyElement = row.findElement(By.xpath("./td[3]"));
                        String companyCurrency = companyCurrencyElement.getText();
                        System.out.println(companyCurrency);

                        WebElement companySymbolElement = row.findElement(By.xpath("./td[2]"));
                        String companySymbol = companySymbolElement.getText().trim().replaceAll("\\s", ".");
                        String companySymbolAlt = companySymbolElement.getText().trim().replaceAll("\\s", "_");
                        System.out.println(companySymbol);
                        System.out.println(companySymbolAlt);

                        if (companyCurrency.equals("NOK"))
                        {
                            companySymbol = companySymbol.substring(0, companySymbol.length() - 1);
                            System.out.println(companySymbol);
                        }

                        WebElement companyNrElement = row.findElement(By.xpath("./td[4]"));
                        String companyNr = companyNrElement.getText();
                        System.out.println(companyNr);

                        stockCompany.setName(companyName);
                        stockCompany.setSymbol(companySymbol);
                        stockCompany.setSymbolAlt(companySymbolAlt);
                        stockCompany.setCurrency(companyCurrency);
                        stockCompany.setCompanyNr(companyNr);
                        stockCompany.setList(list.getList());
                        stockCompany.setExchange(list.getExchange());

                        stockCompanyList.add(stockCompany);
                    }
                }

                // Save stocks to db here
                System.out.println("Saving stock companies to db");
                for (StockCompany stockCompany : stockCompanyList)
                {
                    StockCompany company =
                            mongo.getOneStockCompany(stockCompany.getSymbol(), stockCompany.getExchange());
                    if (company == null)
                    {
                        System.out.println("Inserting stockCompany: " + stockCompany);
                        mongo.updateOneStockCompany(stockCompany);
                    }
                    else
                    {
                        System.out.println("No insert as stock exists");
                    }

                }

            }
            catch (Exception e)
            {
                throw new Exception();
            }

            finally
            {
                //Clean up Webdriver
                System.out.println("Cleaning up driver.");
                driver.manage().deleteAllCookies();
                driver.quit();

                // Stop proxy
                //scraperSettings.stopBrowserMobProxy();
            }

        }
    }

    public static void main(String[] args) throws Exception
    {
        ScrapeStockCompaniesNasdaq scrapeStockCompaniesNasdaq = new ScrapeStockCompaniesNasdaq();
        scrapeStockCompaniesNasdaq.getStockPrices();
    }
}
