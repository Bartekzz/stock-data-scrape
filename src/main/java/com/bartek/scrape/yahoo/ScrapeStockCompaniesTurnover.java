package com.bartek.scrape.yahoo;

import com.bartek.model.common.StockCompany;
import com.bartek.model.common.StockList;
import com.bartek.mongodb.Mongo;
import com.bartek.settings.ScraperSettings;
import com.bartek.util.ScrapeUtilNew;
import com.bartek.util.SerializationUtil;
import com.bartek.util.StandardUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ScrapeStockCompaniesTurnover
{

    private Mongo mongo;
    ScraperSettings scraperSettings;

    public ScrapeStockCompaniesTurnover()
    {
        // Init DB
        mongo = Mongo.getMongoClientLocal();
        mongo.getMongoDatabase("stockdata");
    }

    public void getStockTurnover() throws Exception
    {

        List<StockCompany> stockCompanyList =
                mongo.getAllCompanies("nasdaqomx");

        ArrayList whiteList = new ArrayList();
        whiteList.add("https://finance.yahoo.com/.*");
        for (StockCompany stockCompany : stockCompanyList)
        {
            System.out.println("Scraping list: " + stockCompany.getSymbol());

            scraperSettings = new ScraperSettings().getInstanceOfScraperSettings(whiteList);
            WebDriver driver = scraperSettings.init();

            // Ohter settings (and exported to object)
            ScrapeUtilNew scrapeUtilNew = new ScrapeUtilNew();

            // Set site
            String site = "https://finance.yahoo.com/quote/" + StandardUtil.dotToHyphen(stockCompany.getSymbol()) + ".ST";

            // Start scrape
            try
            {
                driver.get(site);

                WebElement priceRangeElement = scrapeUtilNew.scrapeIfElementPresentByXpath(driver, 5,
                        "//*[@data-test='DAYS_RANGE-value']", true);

                String priceRangeText = priceRangeElement.getText();
                System.out.println(priceRangeText);

                Float avgPrice = StandardUtil.extractAndCalculateAvgPriceFromRange(priceRangeText);
                System.out.println(avgPrice);

                WebElement avgVolumeElement = scrapeUtilNew.scrapeIfElementPresentByXpath(driver, 5,
                        "//*[@data-test='AVERAGE_VOLUME_3MONTH-value']", true);

                String avgVolumeText = avgVolumeElement.getText();
                System.out.println(avgVolumeText);

                Integer avgVolume = Integer.parseInt(StandardUtil.commaToEmpty(avgVolumeText));
                System.out.println(avgVolume);


                stockCompany.setAvgPrice(1);
                stockCompany.setAvgVolume(1);

                // Save stocks to db here

                if (stockCompany == null)
                {
                    System.out.println("Updating stockCompany: " + stockCompany);
                    mongo.updateOneStockCompany(stockCompany);
                }
                else
                {
                    System.out.println("No insert as stock exists");
                }

            }
            catch (Exception e)
            {
                throw new Exception();
            }

            finally
            {
                //Clean up Webdriver
                System.out.println("Cleaning up driver.");
                driver.manage().deleteAllCookies();
                driver.quit();

                // Stop proxy
                scraperSettings.stopBrowserMobProxy();
            }

        }
    }

    public static void main(String[] args) throws Exception
    {
        ScrapeStockCompaniesTurnover
                scrapeStockCompaniesTurnover = new ScrapeStockCompaniesTurnover();
        scrapeStockCompaniesTurnover.getStockTurnover();
    }
}
