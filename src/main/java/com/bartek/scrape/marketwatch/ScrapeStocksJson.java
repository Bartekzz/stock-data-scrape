//*** Scrape Intra prices from di.se ***

package com.bartek.scrape.marketwatch;

import com.amazonaws.services.sqs.model.Message;
import com.bartek.actions.ActionsSQS;
import com.bartek.model.common.StockCompany;
import com.bartek.model.common.StockList;
import com.bartek.model.marketwatch.MarketwatchIntraHTMLRaw;
import com.bartek.model.marketwatch.MarketwatchIntraJsonRaw;
import com.bartek.model.morphia.StockDataIntraJson;
import com.bartek.mongodb.Mongo;
import com.bartek.mongodb.Morphia;
import com.bartek.settings.ScraperSettings;
import com.bartek.util.SerializationUtil;
import com.bartek.util.StandardUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.openqa.selenium.WebDriver;

import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ScrapeStocksJson
{

    private Mongo mongo;
    private static ScraperSettings scraperSettings;
    private ActionsSQS actionsSQS;

    public ScrapeStocksJson() throws UnknownHostException
    {
        // Init DB
        mongo = Mongo.getMongoClientLocal();
        mongo.getMongoDatabase("stockdata");
        actionsSQS = new ActionsSQS();

        setScraperConfiguration();
    }

    public void setScraperConfiguration() throws UnknownHostException
    {
        // Configure ScraperSetting
        ArrayList whiteList = new ArrayList();
        whiteList.add("");
        scraperSettings = ScraperSettings.getInstanceOfScraperSettings(whiteList);
    }

    public void saveStocksToSQSQueue(List<StockCompany> stockCompanyList, String queueName)
    {

        actionsSQS.createQueueIfNotExists(queueName);
        stockCompanyList
                .forEach(x -> ActionsSQS.sendMessage(SerializationUtil.stockCompanyToJson(x), queueName, queueName));
    }

    public List<Message> receiveMessagesFromSQSQueue(String queueName)
    {
        return actionsSQS.receiveMessages(queueName);
    }

    public StockCompany messageToStockCompany(Message message)
    {
        return SerializationUtil.stockCompanyFromJson(message.getBody());
    }

    public boolean queueCreated(String queueName)
    {
        return actionsSQS.createQueueIfNotExists(queueName);
    }

    public boolean queueExists(String queueName)
    {
        System.out.println("queueExists: " + actionsSQS.queueExists(queueName));
        return actionsSQS.queueExists(queueName);
    }

    public boolean messagesInQueue(String queueName) throws Exception
    {
        if (actionsSQS.messagesInQueue(queueName) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void deleteMessage(Message message, String queueName)
    {
        actionsSQS.deleteMessages(message.getReceiptHandle(), queueName);
    }

    public void deleteQueue(String queueName) throws Exception
    {
        if (actionsSQS.messagesInQueue(queueName) == 0)
        {
            actionsSQS.deleteQueue(queueName);
        }
    }

    public void consumeMessages(String queueName) throws Exception
    {
        while (messagesInQueue(queueName))
        {
            List<Message> messages = receiveMessagesFromSQSQueue(queueName);
            System.out.println("messages size: " + messages.size());

            for (Message message : messages)
            {
                StockCompany stockCompany = messageToStockCompany(message);
                System.out.println("Getting stock prices for company, " + stockCompany.getSymbol());
                // Start timer
                long startTime = System.nanoTime();

                // Get stockprice
                getStockPrices(stockCompany);
                // Delete message from queue
                deleteMessage(message, queueName);

                // End timer
                long endTime = System.nanoTime();
                long timeElapsed = endTime - startTime;
                System.out.println("Execution time in seconds : " +
                        timeElapsed / 1000000000);
                //TimeUnit.SECONDS.sleep(Math.max(1, 30 - (timeElapsed / 1000000000)));
            }
        }
    }

    public void scrapeStockPrice(String symbol, String list) throws Exception
    {
        long startTime = System.nanoTime();

        getStockPrices(mongo.getOneStockCompany(symbol, list));

        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("Execution time in seconds : " +
                timeElapsed / 1000000000);

        //TimeUnit.SECONDS.sleep(Math.max(1, 20 - (timeElapsed / 1000000000)));
    }

    public void scrapeStockPrices(LocalDate localDate, boolean reRun) throws Exception
    {

        localDate = (localDate == null) ? LocalDate.now() : localDate;

        List<StockList> stockLists = SerializationUtil.stockListFromJson("stocklists.json");

        // Name and create queue
        String queueName = stockLists.get(0).getExchange() + "_" + localDate;
        boolean queueExists = queueExists(queueName);
        System.out.println("queueExists: " + queueExists);
        if (queueExists)
        {
            System.out.println("There is a queue. Consuming messages..");

            consumeMessages(queueName);
            deleteQueue(queueName);
        }
        else
        {
            for (StockList stockList : stockLists)
            {
                // Execute scrape for all companies or rerun for mismatched errors
                List<StockCompany> stockCompanyList;
                if (reRun)
                {
                    stockCompanyList =
                            mongo.getAllCompaniesWithSomeErrors(stockList.getExchange());
                }
                else
                {
                    stockCompanyList =
                            mongo.getAllCompanies(stockList.getExchange());
                }
                Collections.sort(stockCompanyList);
                stockCompanyList.forEach(x -> System.out.println(x.getSymbol() + ", " + x.getExchange()));
                System.out.println("Stock companies #: " + stockCompanyList.size());

                saveStocksToSQSQueue(stockCompanyList, queueName);
                consumeMessages(queueName);
            }
            deleteQueue(queueName);
        }

    }

    public void getStockPrices(StockCompany stockCompany) throws Exception
    {

        // Company
        System.out.println("Scraping: " + stockCompany.getSymbol());

        // Set stockExchange
        String stockExchange = StandardUtil.currencyToStockExchange(stockCompany.getCurrency());

        WebDriver driver;

        while (true)
        {
            int count = 0;
            int maxTries = 3;
            while (true)
            {
                try
                {
                    driver = scraperSettings.init();
                    if (driver != null)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {
                    System.out.println("Scraper init failed. Retrying..");
                    if (++count == maxTries)
                    {
                        System.out.println("Max init retires!");
                        throw e;
                    }
                }
            }

            // Set site
            String site = "";
            System.out.println("Site url: " + site);

            String xml = "";
            // Start scrape
            try
            {
                driver.get(site);
                xml = driver.getPageSource();
                System.out.println("xml: " + xml);
            }
            catch (Exception e)
            {
                System.out.println("Driver get failed.");
                //throw new Exception();
            }

            // Convert XML to JSON
            String jsonPrettyPrintString = "";

            if (xml.contains("html"))
            {

                StandardUtil standardUtil = new StandardUtil();
                try
                {
                    jsonPrettyPrintString = standardUtil.extractJsonFromHtml(xml);
                }
                catch (NullPointerException e)
                {
                    System.out.println("NullPointerException regex [HTML]");
                    stockCompany.setErrorMessage("NullPointerException regex [HTML]");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }

                // Convert JSON to POJO
                MarketwatchIntraHTMLRaw marketwatchIntraHTMLRaw;
                try
                {
                    marketwatchIntraHTMLRaw =
                            SerializationUtil.jsonToPojoMarketwatchIntraHTMLRaw(jsonPrettyPrintString);
                }
                catch (RuntimeException e)
                {
                    System.out.println("Wrong format [HTML])");
                    System.out.println(jsonPrettyPrintString);
                    System.out.println(e);
                    // Set error true and update to db
                    stockCompany.setErrorMessage("Wrong format [HTML])");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }

                StockDataIntraJson stockDataIntraJson;
                try
                {
                    stockDataIntraJson =
                            SerializationUtil.marketwatchIntraHTMLRawToMarketwatchIntraJson(marketwatchIntraHTMLRaw);
                    // If conversion to pojo went through (symbol, dates and prices exist!)
                    String result = Morphia.updateStockDataIntraJson(stockDataIntraJson);
                    System.out.println("Updated result: " + result);

                    //If run as scrapeStockPrice (single stockComapany that failed for some reason), then remove errorMesseage (upddate in db)
                    System.out.println();
                    stockCompany.setErrorMessage(null);
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }
                // Symbol, dates or prices were missing
                catch (RuntimeException e)
                {
                    System.out.println("Missing symbol, dates or prices [HTML]");
                    stockCompany.setErrorMessage("Missing symbol, dates or prices [HTML]");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }
            }
            else
            {
                try
                {
                    JSONObject xmlJSONObj = XML.toJSONObject(xml);
                    jsonPrettyPrintString = xmlJSONObj.toString(4);
                    System.out.println(jsonPrettyPrintString);
                }
                catch (JSONException je)
                {
                    System.out.println("Exception json [JSON]");
                    System.out.println(je.toString());
                    stockCompany.setErrorMessage("Exception json  [JSON]");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }

                if (jsonPrettyPrintString.contains("Error"))
                {
                    System.out.println("Error [JSON]");
                    System.out.println(jsonPrettyPrintString);
                    // Set error true and update to db
                    stockCompany.setErrorMessage("Error [JSON]");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }

                // Convert JSON to POJO
                MarketwatchIntraJsonRaw marketwatchIntraJsonRaw;
                try
                {
                    marketwatchIntraJsonRaw =
                            SerializationUtil.jsonToPojoMarketwatchIntraJsonRaw(jsonPrettyPrintString);
                }
                catch (RuntimeException e)
                {
                    System.out.println("Wrong json [JSON]");
                    System.out.println(jsonPrettyPrintString);
                    System.out.println(e);
                    // Set error true and update to db
                    stockCompany.setErrorMessage("Wrong json [JSON]");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }

                StockDataIntraJson stockDataIntraJson;
                try
                {
                    stockDataIntraJson =
                            SerializationUtil.marketwatchIntraJsonRawToMarketwatchIntraJson(marketwatchIntraJsonRaw);
                    // If conversion to pojo went through (symbol, dates and prices exist!)
                    String result = Morphia.updateStockDataIntraJson(stockDataIntraJson);
                    System.out.println("Updated result: " + result);

                    //If run as scrapeStockPrice (single stockComapany that failed for some reason), then remove errorMesseage (upddate in db)
                    System.out.println();
                    stockCompany.setErrorMessage(null);
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }
                // Symbol, dates or prices were missing
                catch (RuntimeException e)
                {
                    System.out.println("Missing symbol, dates or prices [JSON]");
                    stockCompany.setErrorMessage("Missing symbol, dates or prices [JSON]");
                    mongo.updateOneStockCompany(stockCompany);
                    break;
                }
            }

        }

        //Clean up Webdriver
        System.out.println("Cleaning up driver.");
        driver.manage().deleteAllCookies();
        driver.quit();

        // Stop proxy
        scraperSettings.stopBrowserMobProxy();

    }

    public static void main(String[] args) throws Exception
    {
        ScrapeStocksJson scrapeStocksJson = new ScrapeStocksJson();


        while(true) {
            scrapeStocksJson.scrapeStockPrices(null, false);
            System.out.println("Sleep for 60 sec");
            TimeUnit.SECONDS.sleep(60);
            List<StockCompany> stockCompaniesWithSomeErrors =
                    Mongo.getMongoClientLocal().getAllCompaniesWithSomeErrors("nasdaqomx");
            if(stockCompaniesWithSomeErrors.size() != 0) {
                scrapeStocksJson.scrapeStockPrices(null, true);
            }
            else {
                break;
            }
        }


        //scrapeMarketwatchJson.scrapeStockPrices(LocalDate.of(2020, 06, 29));
        //scrapeMarketwatchJson.scrapeStockPrice("INTRUM", "nasdaqomx");
    }

}
